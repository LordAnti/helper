package com.example.carsharing.ui.waittimer

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.os.Bundle
import android.os.CountDownTimer
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.example.carsharing.R
import com.example.carsharing.api.RetrofitClient
import com.example.carsharing.model.Book
import kotlinx.android.synthetic.main.fragment_wait_timer.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.time.ExperimentalTime

class WaitTimerFragment : Fragment() {

    /**TODO:
     *  Выполнить доработку расчета тайммера (вычитать от требуемого времени время, которое осталось)
     *  Для старта Drive timer установить значение в 00:00
     */


    lateinit var navController: NavController
    lateinit var waitTimer: String
    lateinit var waitTimerForDB: String
    lateinit var waitPrice: String
    lateinit var car: String
    lateinit var textBar: TextView
    private var counter: CountDownTimer? =null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        waitTimer = arguments!!.getString("wait").toString()
        car = arguments!!.getString("car").toString()
        waitPrice = arguments!!.getString("price").toString()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        textBar = abh.findViewById(R.id.textToolBar)

        stopBtn.setOnClickListener {
            when (textBar.text) {
                "Wait timer" -> {
                    viewWait("Drive timer")
                }
                "Время ожидания" -> {
                    viewWait("Время поездки")
                }
                "Drive timer" -> {
                    viewDrive()
                }
                "Время поездки" -> {
                    viewDrive()
                }
            }
        }

        cancelBtn.setOnClickListener {
            navController.navigate(R.id.drive_to_map)
        }
    }

    private fun viewWait(drive: String) {
        timerDriveWait.visibility = View.GONE
        chronometer.visibility = View.VISIBLE
        cancelBtn.visibility = View.GONE
        textBar.text = drive//"Drive timer"
        counter!!.cancel()
        if(textBar.text == drive){
            chronometer.base = SystemClock.elapsedRealtime()
            chronometer.start()
        }
    }

    private fun viewDrive() {
        chronometer.stop()
        val driveTimer = SystemClock.elapsedRealtime() - chronometer.base
        val drivePrice = (driveTimer.toDouble() / 60000) * 8
        val endWait = waitTimer.toLong() - waitTimerForDB.toLong()
        val book = Book(
            car,
            endWait.toString(),
            driveTimer.toString(),
            waitPrice,
            drivePrice.toString()
        )

        val token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.createBook(
            token,
            book
        ).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(
                call: Call<ResponseBody>,
                response: Response<ResponseBody>
            ) {
                if (response.isSuccessful) {
                    val builder = AlertDialog.Builder(requireActivity())
                    builder.setTitle("Information")
                        .setMessage("Thank you!")
                        .setPositiveButton("OK") { dialog, btn ->
                            navController.navigate(R.id.drive_to_map)
                        }
                    val alert = builder.create()
                    alert.show()
                } else {
                    Toast.makeText(
                        requireActivity(),
                        "Something wrong!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

    @ExperimentalTime
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_wait_timer, container, false)
        waitTimer = (waitTimer.toInt() * 60000).toString()
        waitTimerForDB = waitTimer
        setTimeWait(waitTimer.toLong())
        return root
    }


    private fun setTimeWait(wait: Long) {
        counter = object : CountDownTimer(wait, 1000) {
            override fun onFinish() {
                timerDriveWait.visibility = View.GONE
                chronometer.visibility = View.VISIBLE
                cancelBtn.visibility = View.GONE
                chronometer.start()

                if (textBar.text == "Wait timer") {
                    textBar.text = "Drive timer"
                } else {
                    textBar.text = "Время поездки"
                }
                counter!!.cancel()
            }

            @SuppressLint("SetTextI18n")
            override fun onTick(p0: Long) {
                val sec = ((p0 / 1000) % 60)
                val min = ((p0 / 1000) % 3600) / 60
                val hour = (p0 / 1000) / 3600
                val th = if (hour < 10) "0$hour" else hour
                val tm = if (min < 10) "0$min" else min
                val ts = if (sec < 10) "0$sec" else sec
                if (timerDriveWait != null) {
                    timerDriveWait.text = "$th:$tm:$ts"
                    waitTimerForDB = p0.toString()
                }
            }
        }
        (counter as CountDownTimer).start()
    }


}