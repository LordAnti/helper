package com.example.carsharing.ui.settings

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.BitmapFactory
import android.media.VolumeShaper
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.carsharing.R
import com.example.carsharing.api.RetrofitClient
import com.example.carsharing.model.*
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.nav_header_menus.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.*

class SettingsFragment : Fragment(), View.OnClickListener {

    lateinit var token: String

    private var mySettings: SharedPreferences? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val root = inflater.inflate(R.layout.fragment_settings, container, false)

        val abs = root.findViewById<Toolbar>(R.id.abs)

        abs.inflateMenu(R.menu.items_settings)

        abs.setOnMenuItemClickListener { onOptionsItemSelected(item = it) }

        val btn = abs.findViewById<Button>(R.id.menuBackS)

        btn.setOnClickListener {
            requireActivity().findViewById<DrawerLayout>(R.id.drawer_layout).openDrawer(
                GravityCompat.START
            )
        }

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        token = requireActivity().intent.getStringExtra("token")

        RetrofitClient.instance.getUserAllInfo(token)
            .enqueue(object : Callback<Registration> {
                override fun onFailure(call: Call<Registration>, t: Throwable) {
                    Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<Registration>,
                    response: Response<Registration>
                ) {
                    if (response.isSuccessful) {
                        loginSet.setText(response.body()!!.fio)
                        emailSet.setText(response.body()!!.email)
                    } else {
                        Toast.makeText(
                            requireActivity(),
                            response.message(),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

            })

        updateName.setOnClickListener(this)
        updateEmail.setOnClickListener(this)
        updatePassword.setOnClickListener(this)
        imageLoad.setOnClickListener(this)
        updateAvatar()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mySettings = requireActivity().getSharedPreferences("Lang", Context.MODE_PRIVATE)

        if (ActivityCompat.checkSelfPermission(
                this.requireActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                this.requireActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this.requireActivity(),
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ), 101
            )
            return
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == RESULT_OK) {
                    val selectImage = data!!.data
                    val selected: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
                    val cursor = requireActivity().contentResolver.query(
                        selectImage!!,
                        selected,
                        null,
                        null,
                        null
                    )
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(selected[0])
                    val picturePath = cursor.getString(columnIndex)
                    cursor.close()
                    imageLoad.setImageBitmap(BitmapFactory.decodeFile(picturePath))

                    val updateAvatar = UpdateAvatar(picturePath)
                    RetrofitClient.instance.updateAvatar(token, updateAvatar)
                        .enqueue(object : Callback<ResponseBody> {
                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG)
                                    .show()
                            }

                            override fun onResponse(
                                call: Call<ResponseBody>,
                                response: Response<ResponseBody>
                            ) {
                                if (response.isSuccessful) {
                                    Toast.makeText(
                                        requireActivity(),
                                        "Your photo is update",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    updateAvatar()
                                } else {
                                    Toast.makeText(
                                        requireActivity(),
                                        response.message(),
                                        Toast.LENGTH_LONG
                                    ).show()
                                }
                            }

                        })
                }
            }
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.imageLoad -> {
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                intent.type = "image/*"
                startActivityForResult(intent, 1)
            }
            R.id.updateName -> {
                val name = UpdateName(loginSet.text.toString())
                RetrofitClient.instance.updateName(token, name)
                    .enqueue(object : Callback<ResponseBody> {
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
                        }

                        override fun onResponse(
                            call: Call<ResponseBody>,
                            response: Response<ResponseBody>
                        ) {
                            if (response.isSuccessful) {
                                Toast.makeText(
                                    requireActivity(),
                                    "Name is update",
                                    Toast.LENGTH_LONG
                                ).show()
                                val nav =
                                    requireActivity().findViewById<NavigationView>(R.id.nav_view_header)
                                val fio = nav.findViewById<TextView>(R.id.fioTxt)
                                fio.text = loginSet.text
                            } else {
                                Toast.makeText(
                                    requireActivity(),
                                    response.message(),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }

                    })
            }
            R.id.updateEmail -> {
                val email = UpdateEmail(emailSet.text.toString())
                RetrofitClient.instance.updateEmail(token, email)
                    .enqueue(object : Callback<ResponseBody> {
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
                        }

                        override fun onResponse(
                            call: Call<ResponseBody>,
                            response: Response<ResponseBody>
                        ) {
                            if (response.isSuccessful) {
                                Toast.makeText(
                                    requireActivity(),
                                    "Email is update",
                                    Toast.LENGTH_LONG
                                ).show()
                            } else {
                                Toast.makeText(
                                    requireActivity(),
                                    response.message(),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }

                    })
            }
            R.id.updatePassword -> {
                val password = UpdatePassword(passwordSet.text.toString())
                val builder = AlertDialog.Builder(requireActivity())
                builder.setTitle("Information")
                    .setMessage("Are you sure, that change a password?")
                    .setPositiveButton("Yse") { dialog, btn ->
                        RetrofitClient.instance.updatePassword(token, password)
                            .enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                    Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG)
                                        .show()
                                }

                                override fun onResponse(
                                    call: Call<ResponseBody>,
                                    response: Response<ResponseBody>
                                ) {
                                    if (response.isSuccessful) {
                                        Toast.makeText(
                                            requireActivity(),
                                            "Password is update",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    } else {
                                        Toast.makeText(
                                            requireActivity(),
                                            response.message(),
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }

                            })
                    }.setCancelable(true)
                val alert = builder.create()
                alert.show()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {
                RetrofitClient.instance.userLogout(token)
                    .enqueue(object : Callback<ResponseBody> {
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            Toast.makeText(requireActivity(), t.message, Toast.LENGTH_LONG).show()
                        }

                        override fun onResponse(
                            call: Call<ResponseBody>,
                            response: Response<ResponseBody>
                        ) {
                            if (response.isSuccessful) {
                                requireActivity().finish()
                            } else {
                                Toast.makeText(
                                    requireActivity(),
                                    response.message(),
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        }


                    })
            }
            R.id.eng ->{
                language("en")
            }
            R.id.rus->{
                language("ru")
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun language(lang:String){
        val myEditor = mySettings!!.edit()
        myEditor.putString("lang", lang)
        myEditor.apply()
        requireActivity().finish()
        startActivity(requireActivity().intent)
    }

    private fun updateAvatar() {
        RetrofitClient.instance.getAvatar(token).enqueue(object : Callback<UpdateAvatar> {
            override fun onFailure(call: Call<UpdateAvatar>, t: Throwable) {
                Toast.makeText(
                    requireActivity(),
                    t.message,
                    Toast.LENGTH_LONG
                ).show()
            }

            override fun onResponse(call: Call<UpdateAvatar>, response: Response<UpdateAvatar>) {
                if (response.isSuccessful) {
                    if (requireActivity().imageView != null) {
                        requireActivity().imageView.setImageBitmap(BitmapFactory.decodeFile(response.body()!!.avatar))
                    }
                    imageLoad.setImageBitmap(BitmapFactory.decodeFile(response.body()!!.avatar))
                } else {
                    Toast.makeText(
                        requireActivity(),
                        "Avatar",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

        })
    }

}