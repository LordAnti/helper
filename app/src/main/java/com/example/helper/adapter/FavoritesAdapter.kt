package com.example.helper.adapter

import android.location.Geocoder
import android.os.Build
import android.transition.Slide
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.helper.R
import com.example.helper.api.RetrofitClient
import com.example.helper.model.*
import kotlinx.android.synthetic.main.item_favorites.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class FavoritesAdapter(private val items: ArrayList<Favorites>, token: String) :
    RecyclerView.Adapter<FavoritesAdapter.VH>(), View.OnLongClickListener {

    private val token = token

    private var position: Int = 0

    private var viewGroup: ViewGroup? = null

    override fun onLongClick(p0: View?): Boolean {
        Toast.makeText(viewGroup!!.context, position.toString(), Toast.LENGTH_LONG).show()
        carInfo(p0!!)
        return true
    }

    private fun carInfo(viewReq: View) {
        val carId = CarId(items[position].car_id)

        RetrofitClient.instance.getCarInfo(token, carId).enqueue(object : Callback<Car> {
            override fun onFailure(call: Call<Car>, t: Throwable) {
                Toast.makeText(viewReq.context, t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Car>, response: Response<Car>) {
                if (response.isSuccessful) {
                    val inflater: LayoutInflater = LayoutInflater.from(viewReq.context)

                    val view = inflater.inflate(R.layout.popup_window_favorites, null)

                    val popup = PopupWindow(
                        view,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        true
                    )

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        popup.elevation = 10.0F
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        val slideIn = Slide()
                        slideIn.slideEdge = Gravity.TOP
                        popup.enterTransition = slideIn

                        val slideOut = Slide()
                        slideOut.slideEdge = Gravity.RIGHT
                        popup.exitTransition = slideOut

                    }

                    val car = view.findViewById<TextView>(R.id.markCar)
                    val address = view.findViewById<TextView>(R.id.addressValue)
                    val longitude = view.findViewById<TextView>(R.id.longitudeValue)
                    val latitude = view.findViewById<TextView>(R.id.latitudeValue)

                    car.text = items[position].car_name
                    val geo = Geocoder(viewReq.context, Locale.getDefault())
                    val addressValue = geo.getFromLocation(
                        response.body()!!.longitude.toDouble(),
                        response.body()!!.latitude.toDouble(),
                        1
                    )
                    address.text = addressValue[0].getAddressLine(0)
                    longitude.text = response.body()!!.longitude
                    latitude.text = response.body()!!.latitude

                    popup.showAtLocation(
                        viewReq, // Location to display popup window
                        Gravity.CENTER, // Exact position of layout to display popup
                        0, // X offset
                        0 // Y offset
                    )

                } else {
                    Toast.makeText(viewReq.context, response.message(), Toast.LENGTH_LONG).show()
                }


            }

        })
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        viewGroup = parent
        return VH(parent)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        this.position = position
        holder.bind(items[position])
        holder.itemView.setOnLongClickListener(this)
    }

    fun restoreItem(model: Favorites, position: Int) {
        val fav = Favorite(
            model.car_id,
            model.car_name,
            model.name
        )

        RetrofitClient.instance.createFavorites(token, fav)
            .enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    Log.e("Fail add favorite", t.message)
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        items.add(position, model)
                        notifyItemInserted(position)
                    } else {
                        Log.e("WRONG!", response.body().toString())
                    }
                }

            })
    }

    fun removeItem(position: Int) {
        val removeFav = RemoveFavorites(items[position]._id)
        RetrofitClient.instance.removeFavorites(removeFav).enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.e("Fail remove favorite", t.message)
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    items.removeAt(position)
                    notifyItemRemoved(position)
                    notifyItemRangeChanged(position, items.size)
                } else {
                    Log.e("WRONG!", response.body().toString())
                }
            }

        })
    }

    class VH(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_favorites, parent, false)
    ) {

        fun bind(favorites: Favorites) {
            with(itemView) {
                valueCar.text = favorites.car_name
                valueName.text = favorites.name
            }
        }
    }
}