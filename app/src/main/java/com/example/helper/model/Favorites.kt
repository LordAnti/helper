package com.example.helper.model

data class Favorites (
    val _id: String,
    val user_id: String,
    val car_id:String,
    val car_name:String,
    val name:String
)