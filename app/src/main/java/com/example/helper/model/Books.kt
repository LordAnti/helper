package com.example.helper.model

data class Books(
    val _id: String,
    val user_id: String,
    val car: String,
    val time_wait: String,
    val time_drive: String,
    val wait_price: String,
    val drive_price: String,
    val __v: String
)