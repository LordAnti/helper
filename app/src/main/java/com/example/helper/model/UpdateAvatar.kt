package com.example.helper.model

data class UpdateAvatar(val avatar: String)