package com.example.helper.model

data class Registration (
    val fio: String,
    val email: String,
    val password: String,
    val avatar: String
)