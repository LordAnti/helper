package com.example.helper.model

data class Book (
    val car: String,
    val time_wait: String,
    val time_drive: String,
    val wait_price: String,
    val drive_price: String
)